	
	
	
```golang
// basic req/resp interaction: with zmq
var client, _ = zmq.NewSocket(zmq.REQ)
client.Connect("tcp://localhost:59000")
client.SendMessage(request)
if msg, err := client.RecvMessage(0); err != nil {
	panic(err)
} else {
	return c.JSON(fiber.Map{"Reply": msg[0]})
}
```