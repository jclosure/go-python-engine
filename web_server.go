// @/server/server.go
package main

import (
	"errors"
	"fmt"
	"os"
	"os/exec"
	"os/signal"
	"strings"
	"syscall"
	"time"

	"github.com/gofiber/fiber/v2"
	zmq "github.com/pebbe/zmq4"
)

const (
	REQUEST_TIMEOUT = 5000 * time.Millisecond
	MAX_RETRIES     = 3 //  Before we abandon
)

var socket, _ = zmq.NewSocket(zmq.REQ)

func try_request(request []string) (reply []string, err error) {
	//  Send request, wait safely for reply
	socket.SendMessage(request)
	poller := zmq.NewPoller()
	poller.Add(socket, zmq.POLLIN)
	polled, err := poller.Poll(REQUEST_TIMEOUT)
	reply = []string{}
	if len(polled) == 1 {
		reply, err = socket.RecvMessage(0)
	} else {
		err = errors.New("Time out")
	}
	return
}

func start_engine() *exec.Cmd {
	args := "./python/server.py"
	cmd := exec.Command("python3", strings.Split(args, " ")...)
	cmd.SysProcAttr = &syscall.SysProcAttr{
		Setpgid: true,
	}
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	cmd.Start()
	// cmd.Wait()
	return cmd
}

func register_graceful_shutdown(cmd *exec.Cmd) {
	c := make(chan os.Signal, 1)
	// signal.Notify(c, os.Interrupt)
	signal.Notify(
		c,
		syscall.SIGHUP,
		syscall.SIGINT,
		syscall.SIGTERM,
		syscall.SIGQUIT,
	)
	go func() {
		<-c
		fmt.Println("[force quit]")
		cmd.Process.Kill()
		os.Exit(0)
	}()
}

type Input struct {
	Msg string `json:"msg" xml:"msg" form:"msg"`
}

func main() {
	app := fiber.New()

	cmd := start_engine()

	register_graceful_shutdown(cmd)

	socket.Connect("tcp://localhost:59000")

	app.Post("/api", func(c *fiber.Ctx) error {
		input := new(Input)

		if err := c.BodyParser(input); err != nil {
			panic(err)
		}

		fmt.Printf("server received a message: %v\n", input)
		fmt.Println("sending message to algo")

		request := []string{input.Msg}
		reply, _ := try_request(request)

		fmt.Printf("received reply from algo: %v\n", reply)
		fmt.Println("forwarding reply back to requester")

		return c.JSON(fiber.Map{"Reply": reply[0]})
	})

	app.Listen(":3000")
}
