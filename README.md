#  Example Golang/Python integration via ZMQ

## Requirements
- Go
- Python3

## Setup
```sh
pip3 install -r python/requirements.txt
```

## Usage
Start the server
```sh
go run web_server.go
```

Send it a request
```sh
curl -X POST -H "Content-Type: application/json" --data "{\"msg\":\"foobar\"}" http://localhost:3000/api
```