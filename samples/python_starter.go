package main

import (
	"os"
	"os/exec"
	"strings"
)

func main() {
	args := "./python/server.py"
	cmd := exec.Command("python3", strings.Split(args, " ")...)

	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	cmd.Start()
	cmd.Wait()
}
